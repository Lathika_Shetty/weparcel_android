package com.example.lathikashetty.weparcel;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lathikashetty on 26/9/18.
 */

class OnBoardScreenAdapter extends PagerAdapter implements View.OnClickListener{

    private static final int FIRST_SCREEN = 0;
    private static final int SECOND_SCREEN = 1;
    private static final int THIRD_SCREEN = 2;
    private static final int FOURTH_SCREEN = 3;
    private static final int TOTAL_SIZE = 3;
    private View.OnClickListener mListener;
    FragmentManager fragmentManager;
    public OnBoardScreenAdapter(View.OnClickListener listener, FragmentManager fragmentManager) {
        mListener = listener;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return TOTAL_SIZE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View view = inflater.inflate(R.layout.onboard_screen_item_layout, container, false);

        TextView onboardText = (TextView) view.findViewById(R.id.onboard_text);
       // ImageView getStarted = (ImageView) view.findViewById(R.id.get_started_button);
        TextView headerText = (TextView) view.findViewById(R.id.screen_header);
        ImageView image = (ImageView) view.findViewById(R.id.logo);
        TextView skip = (TextView) view.findViewById(R.id.skip_text);

        skip.setVisibility(View.GONE);
        skip.setOnClickListener(mListener);
      //  getStarted.setVisibility(View.INVISIBLE);
      //  getStarted.setOnClickListener(mListener);
        Context context = onboardText.getContext();
        switch (position) {
            case FIRST_SCREEN:
                 //skip.setVisibility(View.VISIBLE);
//                onboardText.setText(context.getString(R.string.first_onboard_screen_text));
//                headerText.setText(context.getString(R.string.dubber));
                image.setImageResource(R.drawable.onboard1);
                break;
            case SECOND_SCREEN:
//                onboardText.setText(context.getString(R.string.second_onboard_screen_text));
//                headerText.setText(context.getString(R.string.saved_call));
                image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.onboard2));
               // skip.setVisibility(View.VISIBLE);
                break;

            case THIRD_SCREEN:
//                onboardText.setText(context.getString(R.string.fourth_onboard_screen_text));
//                headerText.setText(context.getString(R.string.contact_lists));
                image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.onboard3));
                //getStarted.setVisibility(View.VISIBLE);
                //skip.setVisibility(View.GONE);
                break;
        }

        container.addView(view, 0);
        return view;
    }

    @Override
    public void onClick(View v) {
        /*switch (v.getId()){
            case R.id.get_started_button:

                fragmentManager.beginTransaction().replace(R.id.fragment_container, new LoginFragment()).commitAllowingStateLoss();

                break;
        }*/

    }
}

