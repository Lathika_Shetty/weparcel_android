package com.example.lathikashetty.weparcel;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lathikashetty on 26/9/18.
 */

public class OnBoardScreenFragment extends Fragment implements View.OnClickListener {
    private ViewPager mViewPager;
   // private OnButtonClicked mButtonClicked;
    private CirclePageIndicator mIndicator;

//    public interface OnButtonClicked {
//        void navigateToPlayerScreen();
//    }

    @Override
    public void onAttach(Context ctx) {
     //   Activity activity = (Activity) ctx;
//        try {
//            mButtonClicked = (OnButtonClicked) activity;
//        } catch (ClassCastException e) {
//            e.printStackTrace();
//            throw new ClassCastException(activity.getLocalClassName()
//                    + " must implement OnButtonClicked");
//        }
        super.onAttach(ctx);
     //   ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_onboard_screen, null, false);
       // getContext().getTheme().applyStyle(R.style.FullscreenTheme, true);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FIRST_APPLICATION_WINDOW);
        TextView tv_header2 = view.findViewById(R.id.header2);
        ImageView getStarted = (ImageView) view.findViewById(R.id.get_started_button);
        getStarted.setOnClickListener(this);
        String text = "<font color=#3e4a59>Connect with people\naround</font> "+
                "<font color=#FC4959>you</font>";
        tv_header2.setText(Html.fromHtml(text));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.pager_indicator);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        OnBoardScreenAdapter detailAdapter = new OnBoardScreenAdapter(this,getFragmentManager());
        mViewPager.setAdapter(detailAdapter);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setCurrentItem(0, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.get_started_button:

                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment()).commitAllowingStateLoss();

                break;
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mButtonClicked = null;
        mViewPager = null;
    }
}
