package com.example.lathikashetty.weparcel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by lathikashetty on 27/9/18.
 */

public class OtpFragment extends Fragment  implements View.OnClickListener{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.otp_layout,null,false);
        TextView textView = view.findViewById(R.id.welcome_txt);
        String text = "<font color=#3e4a59>Verify your</font> "+
                "<font color=#FC4959>mobile!</font>";
        textView.setText(Html.fromHtml(text));
/*
        TextView text3 = view.findViewById(R.id.text3);
        String txt = "<font color=#3e4a59>Verify your</font> "+
                "<font color=#FC4959>mobile!</font>";
        text3.setText("To");
*/

        Button verify = view.findViewById(R.id.verify);
        verify.setOnClickListener(this);

        final EditText edt1 = view.findViewById(R.id.edittext1);
        final EditText edt2 = view.findViewById(R.id.edittext2);
        final EditText edt3 = view.findViewById(R.id.edittext3);
        final EditText edt4 = view.findViewById(R.id.edittext4);
//mount the views to java from xml
        edt1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt1.getText().toString().length()==1)     //size as per your requirement
                {
                    edt2.requestFocus();
                    edt2.setEnabled(true);
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        edt3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt3.getText().toString().length()==1)     //size as per your requirement
                {
                    edt4.requestFocus();
                    edt4.setEnabled(true);
                   // edt4.setFocusable(true);
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        edt2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(edt2.getText().toString().length()==1)     //size as per your requirement
                {
                    edt3.requestFocus();
                    edt3.setEnabled(true);
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.verify:


                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new UserNameFragment()).commitAllowingStateLoss();

                break;
        }
    }
}
