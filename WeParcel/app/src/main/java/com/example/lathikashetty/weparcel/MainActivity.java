package com.example.lathikashetty.weparcel;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        dialog = new ProgressDialog(MainActivity.this);
//        dialog.show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_background_color)));
//        dialog.setContentView(R.layout.progressdialog);
//        dialog.setCancelable(false);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        showSplashScreen();

    }


    private void showSplashScreen() {

        final SplashDialogFragment fragment = SplashDialogFragment.newInstance();
//        if (dialog.isShowing()) {
//            dialog.dismiss();
//        }

        fragment.show(getSupportFragmentManager(), "splash");
        // Create a Timer
        Timer RunSplash = new Timer();

        // Task to do when the timer ends
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                // Close SplashScreenActivity.class

             //  MainActivity.this.getSupportActionBar().hide();
                if (fragment != null) {
                   fragment.dismiss();
                }

               // getActionBar().hide();
                // Start MainActivity.class
              //  FragmentUtil.replaceFragment(MainActivity.this, new OnBoardScreenFragment(), R.id.fragment_container);
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.fragment_container, new OnBoardScreenFragment()).commitAllowingStateLoss();
            }
        };

        // Start the timer
        RunSplash.schedule(ShowSplash, 3000);
    }

    protected void hideSplashScreen() {
        // if (mIsSplashVideoCompleted) { // && mIsProfileDataDownloaded
        final SplashDialogFragment fragment = (SplashDialogFragment) getSupportFragmentManager()
                .findFragmentByTag("splash");



    }
}
