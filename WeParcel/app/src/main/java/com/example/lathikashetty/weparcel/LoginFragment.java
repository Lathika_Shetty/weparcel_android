package com.example.lathikashetty.weparcel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by lathikashetty on 27/9/18.
 */

public class LoginFragment extends Fragment implements View.OnClickListener{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.login_layout,null,false);
        TextView textView = view.findViewById(R.id.welcome_txt);
        String text = "<font color=#3e4a59>Welcome</font> "+
                "<font color=#FC4959>back!</font>";
        textView.setText(Html.fromHtml(text));


        Button next = view.findViewById(R.id.next);
        next.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:


                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new OtpFragment()).commitAllowingStateLoss();

                break;
        }
    }
}
