package com.example.lathikashetty.weparcel;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lathikashetty on 26/9/18.
 */

public class SplashDialogFragment extends android.support.v4.app.DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Dialog dialog = new Dialog(getActivity(), R.style.FullscreenTheme);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.splash_layout,null,false);
        dialog.setContentView(view);



        return dialog;
    }

    public static SplashDialogFragment newInstance() {
        return new SplashDialogFragment();
    }
}
